/* Script para comunicaçao serial. Escrito por Pietro Teruya Domingues - 2014
 * */
using UnityEngine;
using System.Collections;
using System.IO.Ports;

public class Serial : MonoBehaviour
{
    public static Serial Instance { get; private set; }

    private void SingletonSetup()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }


    public static SerialPort serial;

	public string port;	
	static bool serialActive = false;
	private static byte[] sendC=new byte[256];
	static int sendCcount=0;

	void Awake () 
	{
        SingletonSetup();
        DontDestroyOnLoad(this);
	

		serial=new SerialPort(port, 115200, Parity.None, 8, StopBits.One); // Setting serial door
		OpenConnection(); // Opening connection

    }

	public static void sendWhenConnected(byte[] bytes)
    {
		foreach(byte eachByte in bytes)
        {
			sendC [sendCcount] = eachByte;
			sendCcount++;
		}
	}

	public void OpenConnection() 
	{
		if (serial != null) // Only do if serial door is not null
		{
			if (serial.IsOpen) // If it is already open, close serial
			{
				serial.Close();
			}
			else 
			{
				serial.Open();  // opens the connection
				serial.ReadTimeout = 100;  // sets the timeout value before reporting error
				serialActive=true; // Activate serial
			}
		}
	}

	public static void send(byte value)
    {
		if(serialActive)
        {
			serial.Write(new byte[]{value},0,1); // Send values to the serial
		}
        else
        {
			sendWhenConnected (new byte[]{value}); // Send only when it connects
		}
	}

	public static void send(string text)
    {
		if (serial != null)
		{
			if (serial.IsOpen) 
			{
				serial.Write (text); // Write to the serial
			}
		}
	}

	public static void send(char[] buffer,int offset,int count)
    {
		if (serial != null)
		{
			if (serial.IsOpen) 
			{
				serial.Write(buffer,offset,count); // Write to the serial
			}
		}
	}

	public static void send(byte[] buffer,int length=0)
    {
		if (serial != null) 
        {
			if (serial.IsOpen) 
            {
				if(length==0)
                {
					length=buffer.Length;
				}
				serial.Write (buffer, 0, length);
			} 
            else
            {
				sendWhenConnected (buffer);
			}
		}
        else 
        {
			sendWhenConnected (buffer);
		}
	}

	public static int read()
	{
		return serial.ReadByte();//rec;
	}

	public static int read2Bytes(byte[] buffer)
    {
		return serial.Read (buffer, 0, 2);
	}

	public static string readLine()
	{
		return serial.ReadLine (); // Read seria line
	}

	void OnApplicationQuit() 
	{
		serial.Close(); // Close door on exiting
	}
}