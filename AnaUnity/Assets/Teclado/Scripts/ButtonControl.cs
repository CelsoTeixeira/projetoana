﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonControl : MonoBehaviour 
{
    public enum ButtonTypes
    {
        keyboard,
        character,
        panel,
        Enter,
        Del,
        Backspace,
        back,
        back2,
        Scene
    };

    public enum SylabeTypes
    {
        Vowel,
        Consonant,
        Other
    };

    public ButtonTypes buttonType;
    public SylabeTypes sylabeType;
    public Text mainText;
    public Text globalText;
    public GameObject panel;
    public string content;
    public Button buttonToGoBack;
    public KeyboardControl keyboardToGoBack;
    public Button firstConsonant;
    public Button firstVowel;

    public int MakeFunction(int index)
    {
        int returnIndex = index;
        switch(buttonType)
        {
            case ButtonTypes.character:
                if(sylabeType == SylabeTypes.Consonant)
                {
                    returnIndex = 9;
                    firstVowel.Select();
                }
                else if(sylabeType == SylabeTypes.Vowel)
                {
                    returnIndex = 0;
                    firstConsonant.Select();
                }
                mainText.text += content;
                break;
            case ButtonTypes.panel:
                panel.SetActive(true);
                break;
            case ButtonTypes.back:
                buttonToGoBack.Select();
                panel.SetActive(false);
                break;
            case ButtonTypes.back2:
                keyboardToGoBack.initialButton.Select();
                panel.SetActive(false);
                break;
            case ButtonTypes.Enter:
                globalText.text += mainText.text;
                mainText.text = "";
                break;
            case ButtonTypes.Backspace:
                if (mainText.text.Length != 0)
                {
                    mainText.text = mainText.text.Substring(0, mainText.text.Length - 1);
                }
                break;
            case ButtonTypes.Scene:
                Application.LoadLevel(0);
                break;
        }
        return returnIndex;
    }
}