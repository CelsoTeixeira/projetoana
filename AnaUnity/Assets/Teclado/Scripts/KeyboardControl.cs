﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class KeyboardControl : MonoBehaviour
{
    public Button initialButton; // Lista de todos os botões do keyboard atual
    public Text textPanel;
    public List<Button> buttons;
    private int index = 0;
    public bool stopCommand = false;
    public bool main = false;
    public KeyboardControl mainKeyboardControl;
    private bool sensorDown = false;

    private IO _io;

    void Awake()
    {
        _io = GameObject.FindGameObjectWithTag("Manager").GetComponent<IO>();
    }

    void OnEnable()
    {
        index = 0;
        StartCoroutine(Highlight());
        if(!main)
        {
            mainKeyboardControl.stopCommand = true;
        }
    }

    IEnumerator Highlight()
    {
        yield return new WaitForSeconds(0.01f);
        buttons[0].Select(); // Selecionar o primeiro botão
    }

    void OnDisable()
    {
        if (!main)
        {
            mainKeyboardControl.stopCommand = false;
        }
    }

    void Update()
    {
        if(stopCommand)
        {
            return;
        }
        
        /*
        if(Input.GetKeyDown(KeyCode.E))
        {
            NextButton();
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            ClickButton();
        }
        */
        //if (!_io.sensorDown)
        //{
        //    if (_io.sensor2 != 0)
        //    {
        //        NextButton();
        //        _io.ButtonPressed();
        //    }
        //    if (_io.sensor1 != 0)
        //    {
        //        ClickButton();
        //        _io.ButtonPressed();
        //    }

        //}

        if (!IO.instance.sensorDown)
        {
            if (IO.instance.sensor2 != 0)
            {
                NextButton();
                IO.instance.ButtonPressed();
            }
            if (IO.instance.sensor1 != 0)
            {
                ClickButton();
                IO.instance.ButtonPressed();
            }
        }
    }

    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(0.2f);
        sensorDown = false;
    }

    void ClickButton()
    {
        SoundManager.instance.PlaySound("Select");
        index = buttons[index].GetComponent<ButtonControl>().MakeFunction(index);
    }

    void NextButton()
    {
        SoundManager.instance.PlaySound("NextButton");
        index++;
        if(index > buttons.Count - 1)
        {
            index = 0;
        }
        buttons[index].Select();
    }
}