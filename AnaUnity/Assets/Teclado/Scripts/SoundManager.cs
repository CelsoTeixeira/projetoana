﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour 
{
    public static SoundManager instance;
    [Tooltip("Audios do teclado: 0 para rolagem de botões e 1 para seleção de botões.")]
    public List<AudioClip> audioList;

    void Awake()
    {
        instance = this;
    }

    public void PlaySound(string type)
    {
        switch(type)
        {
            case "NextButton":
                AudioSource.PlayClipAtPoint(audioList[0], Vector2.zero);
                break;
            case "Select":
                AudioSource.PlayClipAtPoint(audioList[1], Vector2.zero);
                break;
        }
    }
}