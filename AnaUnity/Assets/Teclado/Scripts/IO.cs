﻿/* Script de comunicaçao Arduino/Unity bidirecional. Escrito por Pietro Teruya Domingues e Gilson Domingues - 2015
/ contato: inventaniakits@gmail.com
 * */
using UnityEngine;
using System.Collections;
public class IO : MonoBehaviour 
{
    public static IO instance { get; private set; }
    private void SingletonSetup()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

	int[] inputs = {0,0,0,0,0,0,0,0};
	public bool connect;
	public float sensor1=0;
	public float sensor2=0;
	int saida1=0;
	int saida2=0;
    Rigidbody rigidbody;
    public bool sensorDown = false;
    private bool startedCoroutine = false;

    private bool _sensorDown;

	void Start ()
    {
        SingletonSetup();
        DontDestroyOnLoad(this);

        //Inicia a comunicaçao com a Arduino enviando uma String com dois valores
		Serial.send ("o" + saida1 + ";" + saida2 + "\n");
        rigidbody = GetComponent<Rigidbody>();
        instance = this;

	}



    public void HandlePressButton()
    {
        if(sensorDown)
        {
            if(!startedCoroutine)
            {
                StartCoroutine(CoolDown());
                return;
            }
            return;
        }
    }

    public void ButtonPressed()
    {
        sensorDown = true;
        StartCoroutine(CoolDown());
    }

    IEnumerator CoolDown()
    {
        yield return new WaitForSeconds(0.2f);
        sensorDown = false;
        startedCoroutine = false;
    }

    void Level01()
    {
        ////This controls the button to not be pressed more than one time per press.
      
            if (!_sensorDown)
            {
                if (sensor1 == 1)
                {
                    _sensorDown = true;
                    NavigationSystemController.Instance.YesButton();
                    StartCoroutine(HandleCD());
                    Debug.Log("Yes Button.");
                }
                else if (sensor2 == 1)
                {
                    _sensorDown = true;
                    NavigationSystemController.Instance.NoButton();
                    StartCoroutine(HandleCD());
                    Debug.Log("No Button");
                }
            }
        
    }

    private IEnumerator HandleCD()
    {
        yield return new WaitForSeconds(0.4f);
        _sensorDown = false;
    }

	void Update () 
    {
	    if (Application.loadedLevel == 1)
	    {
            Level01();
	    }

        //HandlePressButton();
	    if (connect) 
        {
	        try
            {
	            // receber os valores da Arduino via comunicaçao serial
		        // recebe em uma linha inteira valores divididos por ; 
			    splitNumber(Serial.readLine());
		    }
            catch(System.TimeoutException)
            {
			    // Se nao consequir fazer nova solicitaçao
			    Serial.send ("o" + saida1 + ";" + saida2 + "\n");
		    }
		}
	}
	void splitNumber(string incoming)
    {
		string[] sep = {"/n",";"};
		string[] strNumber = incoming.Split (sep, System.StringSplitOptions.RemoveEmptyEntries);
		int i = 0;
		foreach (string n in strNumber) 
        {
			inputs[i]=int.Parse(n);
			i++;
		}
		//Agora desmembrar os valores da Array
		sensor1 = (float)inputs [0];
        //print ("Sensor 1 " + sensor1);
		sensor2 = (float)inputs [1];
        //print ("Sensor 2 " + sensor2);
		Serial.send ("o" + saida1 + ";" + saida2 + "\n");
	}
}	