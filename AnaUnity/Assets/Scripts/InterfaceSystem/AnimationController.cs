﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour
{
    private List<ButtonController> _activeButtonControllers;
    private MainTextController _mainTextController;


    void Start()
    {
        _mainTextController =
            GameObject.FindGameObjectWithTag(GeneralInformationHolder.MainTextTag).GetComponent<MainTextController>();
    }


    public void SetupMainTextController(MainTextController controller)
    {
        _mainTextController = controller;
    }

    public void SetupAnimationController(List<ButtonController> controllers)
    {
        _activeButtonControllers = controllers;
    }

    public void TriggerTransition()
    {
        for (int x = 0; x < _activeButtonControllers.Count; x++)
        {
            _activeButtonControllers[x].HandleButtonAnimationState();
        }

        TriggerMainTextTransition();

    }

    public void TriggerMainTextTransition()
    {
        _mainTextController.HandleMainTextAnimationState();
        
    }

}