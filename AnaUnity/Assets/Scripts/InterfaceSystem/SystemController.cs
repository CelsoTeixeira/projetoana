﻿/*  SystemController.cs - Celso Teixeira Vilela - 08/05/2015
 *                          celso6465@terra.com.br  
 */

using System.Collections.Generic;
using UnityEngine;
using System.Collections;


public class SystemController : MonoBehaviour
{
    //TODO: Will turn globals off.
    #region Singleton

    public static SystemController Instance { get; private set; }

    public void SingletonSetup()
    {
        if (Instance != this && Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    #endregion

    //This should be the first panel to appear.
    public string MainPanel;

    public float TimeToWait = 3f;

    public List<GameObject> _activeButtons;
    public List<ButtonController> _activeButtonControllers;
    public MainTextController _activeMainTextController;

    private NavigationSystemController _navigationSystem;
    private PanelInformationController _panelController;
    private AnimationController _animationController;
    private PanelsDatabase _database;

    private bool _insideCoroutine = false;

#region Mono

    void Awake()
    {
        SingletonSetup();

        _navigationSystem = GetComponent<NavigationSystemController>();
        _panelController = GetComponent<PanelInformationController>();
        _animationController = GetComponent<AnimationController>();

        _database = GameObject.FindGameObjectWithTag(GeneralInformationHolder.DatabaseTag).GetComponent<PanelsDatabase>();
    }

    void Start()
    {
        HandleSystemLoop(MainPanel);
    }

#endregion

    //<summary>
    //This is our main loop, we ask for a change panel, we refresh navigation state, we wait for a time before we transition
    //we transition to grayscale, we change the navigation state to allow for input.
    //</summary>

    private IEnumerator SystemLoop(string nameToGo)
    {
        _insideCoroutine = true;    //bool to not start more than one coroutine.

        _navigationSystem.ChangeNavigationState(NavigationState.Transitioning); //Change navigation state.

        _panelController.ChangePanel(_database.GetValueFromDB(nameToGo));   //Ask for a panel transition.

        _activeButtons = _panelController.GetActiveButtons();   //Refresh our reference.
        _activeButtonControllers = _panelController.GetActiveButtonControllers();   //Refresh our reference.
        _activeMainTextController = _panelController.GetMainTextController();   //Refresh our reference.

        _navigationSystem.NavigationSetup(_activeButtons, _activeButtonControllers);    //Setup the navigation.

        //Debug.Log("Waiting for " + TimeToWait + "seconds before we transition to greyscale.");
        yield return new WaitForSeconds(TimeToWait);

        _activeMainTextController._active = false;
        _animationController.TriggerTransition();   //We trigger the transition.
        _animationController.TriggerMainTextTransition();

        _navigationSystem.ChangeNavigationState(NavigationState.OnPanel);   //Change navigation state.

        _insideCoroutine = false;   
    }

    public void HandleSystemLoop(string panelToGo)
    {
        if (!_insideCoroutine)
        {
            StartCoroutine(SystemLoop(panelToGo));
        }
        else
        {
            Debug.Log("Already running system loop.");
        }
    }

}