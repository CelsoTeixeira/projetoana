﻿/*  ButtonController.cs - Celso Teixeira Vilela - 24/05/2015
 *                          celso6465@terra.com.br
 */

using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(ButtonAnimationController))]
public class ButtonController : MonoBehaviour
{
    public string PanelToGo { get; private set; }
    public string PanelToReturn { get; private set; }

    public int Index { get; private set; }

    public bool LoadLevel { get; private set; }
    public int LevelToLoad { get; private set; }

    public bool CurrentButton;


    public bool HaveAnimation;

    private RectTransform _rectTransform;
    private RectTransform _imageRectTransform;
    private Image Image;
    private Image InsideImage;

    private ButtonAnimationController _buttonAnimationController;

#region Mono

    private void Awake()
    {
        _buttonAnimationController = GetComponentInChildren<ButtonAnimationController>();     //Cache the reference.

        _rectTransform = GetComponent<RectTransform>();     //Reference for the rectTransform of the holder.

        Image[] temp = GetComponentsInChildren<Image>();

        Image = temp[0];
        InsideImage = temp[1];

        _imageRectTransform = Image.GetComponent<RectTransform>();      //Reference for the rect transform for the first image in the children, should be what we are using.

        CurrentButton = false;  //just init this.

        InsideImage.gameObject.SetActive(false);    //We are not using the inside image anymore, but it's a nice idea to reuse sprites if needed.
    }

#endregion

    public void SetCurrentStatus(bool status)
    {
        CurrentButton = status;
    }

    public void SetupLevelToLoad(bool load, int level)
    {
        LoadLevel = load;
        LevelToLoad = level;
    }

    public void SetupPosition(Vector2 pos)
    {
        //We need to use anchoredPosition for this to work and not position...
        _rectTransform.anchoredPosition = pos;
    }

    public void SetupImageRectSize(Vector2 size)
    {
        _imageRectTransform.sizeDelta = size;
    }

    public void SetupImage(Sprite sprite)
    {
        Image.sprite = sprite;
    }

    public void SetupInsideImage(Sprite sprite)
    {
        InsideImage.sprite = sprite;
    }

    public void SetupButtonController(string panelToGo, int index)
    {
        PanelToGo = panelToGo;
        Index = index;
    }

    public void SetupAnimationController(AnimatorOverrideController controller)
    {   
        _buttonAnimationController.Setup(controller);
    }

    public void HandleButtonAnimationState()
    {
        _buttonAnimationController.HandleStateTrigger();
    }
}