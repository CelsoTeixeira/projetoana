﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class ButtonAnimationController : MonoBehaviour
{
    private AnimatorOverrideController _controller;
    private Animator _animator;
    private AnimatorStateInfo _animatorStateInfo;

    private Image _image;

    private bool _colorSpriteCondition = true;
    private Sprite _colorSprite;

    private bool _blackWhiteCondition = false;
    private Sprite _blackWhiteSprite;

    private ButtonController _buttonController;

#region Mono
    
    void Awake()
    {
        _buttonController = GetComponentInParent<ButtonController>();

        _animator = GetComponent<Animator>();
        _image = GetComponent<Image>();
    }

    void Update()
    {
        _animatorStateInfo = _animator.GetCurrentAnimatorStateInfo(0);

        //if (_animatorStateInfo.IsName(AnimationInformation.Color))
        //{
        //    if (!_colorSpriteCondition) //If we dont have setup the right sprites
        //    {
        //        _image.sprite = _blackWhiteSprite;   //Change th sprite.
        //        _colorSpriteCondition = true;   //Refresh the condition.
        //    }
        //}
        //else if (_animatorStateInfo.IsName(AnimationInformation.ColorToBW))
        //{
        //    _blackWhiteCondition = false;   //When we transition from Color to BlackWhite we change the black white condition to change the sprite when we enter the state.
        //}
        //else if (_animatorStateInfo.IsName(AnimationInformation.BlackWhite))
        //{
        //    if (!_blackWhiteCondition)  //If we dont have setup the right sprite.
        //    {
        //        _image.sprite = _colorSprite;  //Change the sprite.
        //        _blackWhiteCondition = true;    //Refresh the condition.
        //    }
        //}
        //else if (_animatorStateInfo.IsName(AnimationInformation.BWToColor))
        //{
        //    _colorSpriteCondition = false; //When we transition from BlackWhite to color we change the color condition to change the sprite when we enter the state.
        //}
    }

#endregion

    public void Setup(AnimatorOverrideController cont)
    {
        _controller = cont;     

        _animator.runtimeAnimatorController = _controller;  //Setup the animator controller in runtime.

    }

    public void HandleStateTrigger()
    {

        //<summary>
        //  If current button = false and we are in the Color state we trigger to go to black white state.
        //  If current button = true and we are in the BW state we trigger to go to color state.
        //</summary>

        if (!_buttonController.CurrentButton)
        {
            if (_animatorStateInfo.IsName(AnimationInformation.Color))
            {
                _animator.SetTrigger(AnimationInformation.TriggerColorBW);
            }
        }
        else if (_buttonController.CurrentButton)
        {
            if (_animatorStateInfo.IsName(AnimationInformation.BlackWhite))
            {
                _animator.SetTrigger(AnimationInformation.TriggerBWColor);
            }
        }
    }

}