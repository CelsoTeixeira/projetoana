﻿/*  PanelsDatabase - Celso Teixeira Vilela - 07 / 05 / 2015
 *                      celso6465@terra.com.br
 *  
 *  Aqui temos uma classe central onde guardamos todos os CustomPanelInformation usados pelo Sistema.
 *  Por enquanto precisamos colocar esses paineis manualmente.
 *  
 *  TODO: Carregar da pasta resources todos os paineis e adiciona-los no _dictionary.
 */

using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class PanelsDatabase : MonoBehaviour
{
    #region Singleton

    public static PanelsDatabase Instance { get; private set; }

    private void SingletonSetup()
    {
        if (Instance != this && Instance != null)
        {
            Destroy(this);
        }

        Instance = this;
    }

    #endregion

    //TODO: Do some load from the resources folder.

    [Tooltip("Each panel we want to be in the dictionary need to be added here.")]
    public List<CustomPanelInformation> PanelsToBeAddedInTheDatabase;

    private Dictionary<string, CustomPanelInformation> _dictionary = new Dictionary<string, CustomPanelInformation>();

    //This is here just to make sure we have the right tag.
    private string Tag = GeneralInformationHolder.DatabaseTag;

    #region Mono

    void Awake()
    {
        SingletonSetup();

        //Populating the dictionary.
        for (var x = 0; x < PanelsToBeAddedInTheDatabase.Count; x++)
        {
            _dictionary.Add(PanelsToBeAddedInTheDatabase[x].PanelID, PanelsToBeAddedInTheDatabase[x]);
        }

        //We just set this tag here.
        this.gameObject.tag = Tag;
    }

    #endregion

    //Getting value from the dictionary with the right key.
    public CustomPanelInformation GetValueFromDB(string key)
    {
        if (_dictionary.ContainsKey(key))
        {
            //Debug.Log("Returning the CustomPanel");
            
            //return the panel respective with the name.
            return _dictionary[key];
        }
        else
        {
            Debug.Log("Nós não temos a key passada, será retornado o PainelPrincipal.");
            return _dictionary["Painel1"];
        }
    }
}