﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class BallonController : MonoBehaviour
{
    private Animator _animator;

    private RectTransform _rectTransform;

    private Image _image;

    public List<AnimatorOverrideController> OverrideControllers { get; private set; }

    public List<Vector2> BallonPosition { get; private set; }
    public List<Vector2> BallonDeltaSize { get; private set; }

    public bool Static { get; private set; }

    void Awake()
    {
        OverrideControllers = new List<AnimatorOverrideController>();
        BallonPosition = new List<Vector2>();
        BallonDeltaSize = new List<Vector2>();

        _animator = GetComponent<Animator>();
        _rectTransform = GetComponent<RectTransform>();
        _image = GetComponent<Image>();
    }

    public void DeactiveImage()
    {
        _image.gameObject.SetActive(false);
    }

    public void ActiveImage()
    {
        _image.gameObject.SetActive(true);
    }

    public void AddController(AnimatorOverrideController cont)
    {
        OverrideControllers.Add(cont);
    }

    public void SetStaticCondition(bool b)
    {
        Static = b;
    }

    public void ClearLists()
    {
        if (OverrideControllers!= null)
            OverrideControllers.Clear();

        if (BallonPosition != null)
            BallonPosition.Clear();

        if (BallonDeltaSize!=null)
            BallonDeltaSize.Clear();
    }

    public void AddPosition(Vector2 pos)
    {
        BallonPosition.Add(pos);
    }

    public void AddSize(Vector2 size)
    {
        BallonDeltaSize.Add(size);
    }

    public void ChangeController(int index)
    {
        _animator.runtimeAnimatorController = OverrideControllers[index];
    }

    public void ChangePosition(int index)
    {
        _rectTransform.anchoredPosition = BallonPosition[index];
    }

    public void ChangeDeltaSize(int index)
    {
        _rectTransform.sizeDelta = BallonDeltaSize[index];
    }
}