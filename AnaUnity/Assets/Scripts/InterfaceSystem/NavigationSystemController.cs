﻿/*  NavigationSystemController.cs - Celso Teixeira Vilela - 08/05/2015
 *                                  celso6465@terra.com.br
 *                                  
 *  Esta clase cuida da navegacao do sistema.
 *  So aceitamos Input se estivermos ou no estado OnPanel ou DisplayingText(Nada aqui por enquanto),
 *  Temos dois tipos de navegacao.
 *  Manual:
 *      O botao atual 'e mudado quando 'e apertado o botao NAO, toda vez que o NAO 'e apertado nos
 *      aumentamos o _currentIndex e atualizamos a referencia do botao.
 *      Quando o SIM 'e apertado nos checamos as informacoes do InformationHolder correspondente e 
 *      agimos de acordo com ela.
 *  Automatica:
 *      O botao atual 'e mudado de acordo com um tempo, sempre que chegamos ao tempo limite nos
 *      aumentamos o index e atualizamos a referencia do botao.
 *      Quando o SIM 'e apertado nos checamos as informacoes do InformationHolder correspondente e 
 *      agimos de acordo com ela.
 *      Quando o NAO 'e apertado nao aumentamos o _currentIndex e atualizamos a referencia do botao.
 * 
 *  Temos um estado interno para controlarmos onde estamos, se estamos em um painel ou se chegamos ao
 *  final de um ponto da arvore de informacao.
 *  
 *  Botoes SIM e NAO sao botoes fisicos, IO.cs 'e onde chama os methods respectivos a cada um.
 */

using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum NavigationState
{
    None,
    OnPanel,    //If we're we need to start the timer and loop between the buttons.
    //TODO:This displayingText needs a better name.
    DisplayingText,  //If we are here, we can just finish the text fast if we press the button.
    Transitioning   //If we're here we don't do anything with buttons, we just wait till we are on the right state.
};


public class NavigationSystemController : MonoBehaviour
{
    #region Singleton

    public static NavigationSystemController Instance { get; private set; }

    public void SingletonSetup()
    {
        if (Instance != this && Instance != null)
            Destroy(this);

        Instance = this;
    }

    #endregion

    private NavigationState _state = NavigationState.None;

    private List<GameObject> _buttons;
    private List<ButtonController> _controllers;

    private ButtonController _currentController;
    private GameObject _currentButton;
    private int _currentIndex;  //We use this to control the current button we are using.

    private AnimationController _animationController;
    private BallonController _ballonController;

#region Mono

    void Awake()
    {
        SingletonSetup();

        _animationController = GetComponent<AnimationController>();

        _ballonController = GameObject.FindGameObjectWithTag(GeneralInformationHolder.Balloon).GetComponent<BallonController>();
    }

#endregion

#region Private methods

    //This is where we control what button we're using.
    private void IncreaseIndex()
    {
        _currentIndex += 1; //Increase the index.

        if (_currentIndex > _buttons.Count - 1)    //Internal control to stay in the array range.
        {
            _currentIndex = 0;
        }

        _currentController.SetCurrentStatus(false);

        _currentButton = _buttons[_currentIndex];   //Refreshing the _currentButton.
        _currentController = _controllers[_currentIndex];

        _currentController.SetCurrentStatus(true); //Set the current status for the first one to be true.

        _animationController.TriggerTransition();

        if (_currentController.HaveAnimation)
        {
            _ballonController.ChangeController(_currentIndex);  //We change the controller.
            _ballonController.ChangePosition(_currentIndex);    //We change the position.
            _ballonController.ChangeDeltaSize(_currentIndex);   //We change the size.
        }
        else if (!_currentController.HaveAnimation && !_ballonController.Static)
        {
            _ballonController.DeactiveImage();
        }

        //Debug.Log("Current button", _currentButton);
    }

#endregion


#region Buttons
    
    public void YesButton()
    {
        Debug.Log("Yes Button pressed.");

        switch (_state)
        {
            case NavigationState.OnPanel:

                if (!_controllers[_currentIndex].LoadLevel) //If we dont load a scene.
                {
                    SystemController.Instance.HandleSystemLoop(_controllers[_currentIndex].PanelToGo);
                    SoundController.Instance.PlayFeedbackSelectionSound();    
                }
                else if (_controllers[_currentIndex].LoadLevel)     //If we need to load a scene.
                {
                    SoundController.Instance.PlayFeedbackSelectionSound();
                    Application.LoadLevel(_controllers[_currentIndex].LevelToLoad);
                }

                break;
        }
    }

    public void NoButton()
    {
        Debug.Log("No Button pressed.");
        
        switch (_state)
        {
            //If we are inside a Panel we increase the index to go to the next button.
            case NavigationState.OnPanel:
                IncreaseIndex();
                SoundController.Instance.PlayFeedbackFowardSound();
                break;
        }
    }

#endregion

    //We use this to setup the information we need to control navigation.
    public void NavigationSetup(List<GameObject> go, List<ButtonController> controllers)
    {
        _buttons = go;
        _controllers = controllers;
        _currentButton = _buttons[0];
        _currentController = controllers[0];
        _currentIndex = 0;

        for (int x = 0; x < _controllers.Count; x++) //For every controller we set the flag to false.
        {
            _controllers[x].SetCurrentStatus(false);    
        }

        _currentController.SetCurrentStatus(true);
        

        _animationController.SetupAnimationController(_controllers);

        if (_currentController.HaveAnimation)
        {
            _ballonController.ChangePosition(_currentIndex);
            _ballonController.ChangeDeltaSize(_currentIndex);
            _ballonController.ChangeController(_currentIndex);

            _ballonController.ActiveImage();
        }

    }

    //We use this to actualize our NavigationState.
    public void ChangeNavigationState(NavigationState state)
    {
        //Debug.Log("Changing NavigationState: " + "FROM - " + _state + " / TO: " + state);
        _state = state;
    }

}