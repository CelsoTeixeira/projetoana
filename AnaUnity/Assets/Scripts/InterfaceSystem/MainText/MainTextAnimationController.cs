﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class MainTextAnimationController : MonoBehaviour
{
    private AnimatorOverrideController _controller;
    private Animator _animator;
    private AnimatorStateInfo _animatorStateInfo;

    private Image _image;

    private bool _colorSpriteCondition = true;
    private Sprite _colorSprite;

    private bool _blackWhiteCondition = false;
    private Sprite _blackWhiteSprite;

    private MainTextController _mainController;


#region Mono

    void Awake()
    {
        _mainController = GetComponent<MainTextController>();

        _animator = GetComponent<Animator>();
        _image = GetComponent<Image>();
    }

    void Update()
    {
        _animatorStateInfo = _animator.GetCurrentAnimatorStateInfo(0);
    }

#endregion

    public void Setup(AnimatorOverrideController cont)
    {
        _controller = cont;

        _animator.runtimeAnimatorController = _controller;
    }

    public void HandleStateTrigger()
    {
        if (!_mainController._active)
        {
            if (_animatorStateInfo.IsName(AnimationInformation.Color))
            {
                _animator.SetTrigger(AnimationInformation.TriggerColorBW);
            }
        }
        else if (_mainController._active)
        {
            if (_animatorStateInfo.IsName(AnimationInformation.BlackWhite))
            {
                _animator.SetTrigger(AnimationInformation.TriggerBWColor);
            }
        }

    }

}
