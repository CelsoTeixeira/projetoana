﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(MainTextAnimationController))]
public class MainTextController : MonoBehaviour
{
    private RectTransform _rectTransform;
    private Image _image;

    private MainTextAnimationController _animationController;
    public bool _active = true;

    void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        _image = GetComponent<Image>();

        _animationController = GetComponent<MainTextAnimationController>();
    }

    public void SetupPosition(Vector2 pos)
    {
        _rectTransform.anchoredPosition = pos;
    }

    public void SetupImageRectSize(Vector2 size)
    {
        _rectTransform.sizeDelta = size;
    }

    public void SetupImage(Sprite sprite)
    {
        _image.sprite = sprite;
    }

    public void SetupAnimationController(AnimatorOverrideController controller)
    {
        _animationController.Setup(controller);
    }

    public void HandleMainTextAnimationState()
    {
        _animationController.HandleStateTrigger();
    }
}