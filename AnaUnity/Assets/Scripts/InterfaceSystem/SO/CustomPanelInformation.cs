﻿/*  CustomPanelInformation.cs - Celso Teixeira Vilela - 08/05/2015
 *                                  celso6465@terra.com.br 
 */

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class CustomPanelInformation : ScriptableObject
{
    [Tooltip("The unique ID for the panel. This is used when we need to initialize a new panel.")]
    [SerializeField] public string PanelID;  //The unique ID for the panel, we use for reference in the dictionary.

    [Tooltip("The background sprite we will use in the panel.")]
    [SerializeField] public Sprite Background;      //The background we are going to use in the panel;

    [Header("Buttons settings")]
    [Tooltip("The number of buttons the panel need.")]
    [SerializeField] public int NumberOfButtons;    //The number of buttons we are going to have active in the canvas.

    [Tooltip("The position for each button.")]
    [SerializeField] public List<Vector2> ButtonsPositions;     //The position for each button in the canvas.

    [Tooltip("If the buttons need to have diferent sizes.")]
    [SerializeField] public bool DiferentButtonsSize;   //If we have different buttons size.

    [Tooltip("The size for each button. If we don't have diferent sizes the first one in this list will be the default for every button.")]
    [SerializeField] public List<Vector2> ButtonsRectSize;  //The size for each button. The default will be the first in the list.

    [Tooltip("The animator controller for each button.")]
    [SerializeField] public List<AnimatorOverrideController> ButtonsControllers;

    [SerializeField] public bool HaveStaticAnimation = false;

    [SerializeField] public AnimatorOverrideController StaticController;

    [SerializeField] public Vector2 StaticAnimationPosistion;

    [SerializeField] public Vector2 StaticAnimationDeltaSize;

    [SerializeField] public bool HaveMultipleAnimations = false;

    [SerializeField] public List<bool> HaveAnimation;

    [SerializeField] public List<AnimatorOverrideController> NewAnimation;

    [SerializeField] public List<Vector2> AnimationPosition;

    [SerializeField] public List<Vector2> AnimationRectSize;

    [Tooltip("The panel we will go when we press the button.")]
    [SerializeField] public List<string> PanelIDToGo;   //The unique id for the panel. This is used in the dictionary.

    [SerializeField] public List<bool> LoadLevel;

    [SerializeField] public List<int> LevelToLoad;

    [Header("Middle Text settings")]
    [Tooltip("The sprite we will use for the middle text.")]
    [SerializeField] public Sprite MiddleTextColorSprite;  //The sprite we will use for the middle text.

    [SerializeField] public Sprite MiddleTextBWSprite;

    [SerializeField] public AnimatorOverrideController MiddleTextController;

    [Tooltip("The position of the middle text. If we don't need to change the position we use (0, 0) as the default one.")]
    [SerializeField] public Vector2 MiddleTextPosition; //The position of the middle text.

    [SerializeField] public Vector2 MiddleTextDeltaSize;


}