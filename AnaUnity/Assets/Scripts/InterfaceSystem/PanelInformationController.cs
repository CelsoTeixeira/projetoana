﻿/*  PanelInformationController.cs - Celso Teixeira Vilela - 08/05/2016
 *                                  celso6465@terra.com.br
 */

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PanelInformationController : MonoBehaviour
{
    //TODO: I don't know if I want this to have global access.

    #region Singleton

    public static PanelInformationController Instance { get; private set; }

    public void SingletonSetup()
    {
        if (Instance != this && Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    #endregion

    private Vector2 DefaultPosition = Vector2.zero;

    private List<GameObject> ButtonsGameObjects = new List<GameObject>(9); //Contains all our buttons. 
    private List<ButtonController> ButtonsController = new List<ButtonController>(9); //Contains all our controllers.

    private MainTextController _mainTextController;

    private Image MainText; //Middle text image reference.
    private Image Background; //Background image reference.

    private CustomPanelInformation _currentPanelInformation; //We track our current panel.
    private List<GameObject> _activeButtons = new List<GameObject>(9); //Track for the current buttons.
    private List<ButtonController> _buttonControllers = new List<ButtonController>(9); //Track for current controllers.

    private BallonController _ballonController;

    private bool _insideCoroutine = false; //If we are already changing the panel with the coroutine.

    #region Mono

    private void Awake()
    {
        SingletonSetup();

        //We get all the references here.
        MainText = GameObject.FindGameObjectWithTag(GeneralInformationHolder.MainTextTag).GetComponent<Image>();
        Background = GameObject.FindGameObjectWithTag(GeneralInformationHolder.BackgroundTag).GetComponent<Image>();
        _ballonController = GameObject.FindGameObjectWithTag(GeneralInformationHolder.Balloon).GetComponent<BallonController>();

        //We get all our buttons.
        ButtonsGameObjects = GameObject.FindGameObjectsWithTag(GeneralInformationHolder.ButtonTag).ToList();

        for (int x = 0; x < ButtonsGameObjects.Count; x++)
        {
            ButtonsController.Add(ButtonsGameObjects[x].GetComponent<ButtonController>());
        }

        _mainTextController = MainText.GetComponent<MainTextController>();
    }

    private void Start()
    {
        //We need to call it on Start to make sure we have all the references for each component we need.
        DeActiveAllButtons();
    }

    #endregion

    //DeActive the buttons and send back to (0,0)
    private void DeActiveAllButtons()
    {
        for (int x = 0; x < ButtonsGameObjects.Count; x++)
        {
            ButtonsGameObjects[x].SetActive(false); //Deactive the button.

            ButtonsController[x].SetupPosition(DefaultPosition); //We send back to (0,0).
        }
    }

    private void SetupButtons()
    {
        for (int x = 0; x < _currentPanelInformation.NumberOfButtons; x++)
        {
            //Set the button active.
            ButtonsGameObjects[x].SetActive(true);

            //Add to a internal track.
            _activeButtons.Add(ButtonsGameObjects[x]);
            _buttonControllers.Add(ButtonsController[x]);

            ButtonsController[x].SetupPosition(_currentPanelInformation.ButtonsPositions[x]); //Refresh the position.

            ButtonsController[x].SetCurrentStatus(false);   //Set all the current status to false.

            if (_currentPanelInformation.DiferentButtonsSize)
                //We use this to control if we will change the size of each button or not.
            {
                ButtonsController[x].SetupImageRectSize(_currentPanelInformation.ButtonsRectSize[x]);
                    //If we are we just loop the same way we have been doing.
            }
            else
            {
                ButtonsController[x].SetupImageRectSize(_currentPanelInformation.ButtonsRectSize[0]);
                    //If not we get the first position in the list.
            }

            ButtonsController[x].SetupAnimationController(_currentPanelInformation.ButtonsControllers[x]);

            ButtonsController[x].SetupButtonController(_currentPanelInformation.PanelIDToGo[x], x); //Refresh the information

            ButtonsController[x].SetupLevelToLoad(_currentPanelInformation.LoadLevel[x], _currentPanelInformation.LevelToLoad[x]);

            if (_currentPanelInformation.HaveAnimation[x])
            {
                //Setup the animation here.
                _ballonController.AddController(_currentPanelInformation.NewAnimation[x]);
                _ballonController.AddPosition(_currentPanelInformation.AnimationPosition[x]);
                _ballonController.AddSize(_currentPanelInformation.AnimationRectSize[x]);

                _ballonController.SetStaticCondition(false);
            }

            ButtonsController[x].HaveAnimation = _currentPanelInformation.HaveAnimation[x];

            //TODO:Maybe we need to deactive the balloon image here.

        }
    }

    private void SetupMiddleTextBackground()
    {
        Background.sprite = _currentPanelInformation.Background; //We refresh the background.

        _mainTextController.SetupPosition(_currentPanelInformation.MiddleTextPosition); //Change the position

        _mainTextController.SetupImageRectSize(_currentPanelInformation.MiddleTextDeltaSize); //Change the deltaSize

        _mainTextController.SetupAnimationController(_currentPanelInformation.MiddleTextController);

        //TODO:Check this part
        _mainTextController._active = true;

        _mainTextController.HandleMainTextAnimationState();
    }

    public List<GameObject> GetActiveButtons()
    {
        return _activeButtons;
    }

    public List<ButtonController> GetActiveButtonControllers()
    {
        return _buttonControllers;
    }

    public MainTextController GetMainTextController()
    {
        return _mainTextController;
    }

//This is the core to change the panel.
    public void ChangePanel(CustomPanelInformation panel)
    {
        //NavigationSystemController.Instance.ChangeNavigationState(NavigationState.Transitioning);   //Refresh the navigationState.

        _activeButtons.Clear(); //Clear list.
        _buttonControllers.Clear(); //Clear list.

        _ballonController.ClearLists();

        _currentPanelInformation = panel; //Refresh the currentPanel.

        DeActiveAllButtons(); //Disable all the buttons.

        SetupMiddleTextBackground();
        SetupButtons(); //Refresh all the information with a loop.

        if (_currentPanelInformation.HaveStaticAnimation)
        {
            _ballonController.SetStaticCondition(true);

            _ballonController.AddController(_currentPanelInformation.StaticController);
            _ballonController.AddPosition(_currentPanelInformation.StaticAnimationPosistion);
            _ballonController.AddSize(_currentPanelInformation.StaticAnimationDeltaSize);
            _ballonController.ChangeController(0);
            _ballonController.ChangePosition(0);
            _ballonController.ChangeDeltaSize(0);
        }
        else if(!_currentPanelInformation.HaveStaticAnimation && !_currentPanelInformation.HaveMultipleAnimations)
        {
            _ballonController.DeactiveImage();
        }

        //NavigationSystemController.Instance.NavigationSetup(_activeButtons, _buttonControllers); //Refresh the navigation system.
        //NavigationSystemController.Instance.ChangeNavigationState(NavigationState.OnPanel); //Refresh the NavigationState.
    }


    //This is a test.
    //private IEnumerator ChangePanelByStep(CustomPanelInformation panel)
    //{
    //    _insideCoroutine = true;
    
    //    //Debug.Log("We are changing the panel.");
    
    //    //We actualize the NavigationState
    //    NavigationSystemController.Instance.ChangeNavigationState(NavigationState.Transitioning);
    
    //    //We clear the active buttons.
    //    _activeButtons.Clear();
    //    _informationHolders.Clear();
    
    //    //We cache the current panel.
    
    //    _currentPanelInformation = panel;

    //    //_mainTextAnimationHolder.BoolAnimation(AnimationInformation.FadeOut, true);
    //    //yield return new WaitForSeconds(AnimationInformation.AnimationTime);
    //    //_mainTextAnimationHolder.BoolAnimation(AnimationInformation.FadeOut, false);

    //    //MainText.gameObject.SetActive(false);

    //    //Deactive the buttons to make sure we only have the necessary.
    //    DeActiveAllButtons();
    
    //    //We refresh the mainText.
    //    MainText.overrideSprite = _currentPanelInformation.MiddleText;
    //    _mainTextAnimationHolder.BoolAnimation(AnimationInformation.FadeIn, true);
    
    //    MainText.gameObject.SetActive(true);
    
    //    yield return new WaitForSeconds(AnimationInformation.AnimationTime);
    
    //    //We setup the navigation system.
    //    NavigationSystemController.Instance.NavigationSetup(_activeButtons.ToArray(), _informationHolders.ToArray());
    
    //    //We actualize the NavigationState
    //    NavigationSystemController.Instance.ChangeNavigationState(NavigationState.OnPanel);
    
    //    _insideCoroutine = false;
    //
    //}

    //public void CallChagePanelByStep(CustomPanelInformation panelToChange)
    //{
    //    if (!_insideCoroutine)
    //        StartCoroutine(ChangePanelByStep(panelToChange));
    //    else
    //        Debug.Log("inside the coroutine already.");
    //}

    //TODO:Change this in a coroutine to make better transition.

}