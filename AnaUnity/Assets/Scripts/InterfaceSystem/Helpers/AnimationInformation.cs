﻿using UnityEngine;
using System.Collections;

//We hold every transition state here to not have floating string in the code.

public static class AnimationInformation
{
    public const string Color = "Color";
    public const string BlackWhite = "BlackWhite";
    public const string ColorToBW = "ColorToBlackWhite";
    public const string BWToColor = "BlackWhiteToColor";
    
    public const string TriggerColorBW = "ColorToBW";
    public const string TriggerBWColor = "BWToColor";

    public const float AnimationTime = 1.0f;
}