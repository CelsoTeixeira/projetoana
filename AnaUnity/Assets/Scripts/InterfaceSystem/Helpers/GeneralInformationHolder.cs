﻿using UnityEngine;
using System.Collections;

//We will store every empty string here.
public static class GeneralInformationHolder
{
    public static string ButtonTag = "Button";
    public static string DatabaseTag = "Database";
    public static string MainTextTag = "MainText";
    public static string BackgroundTag = "Background";
    public static string SoundControl = "SoundControl";
    public static string AmbienceSound = "AmbienceSound";
    public static string FeedbackSound = "FeedbackSound";
    public static string Balloon = "Balloon";

}