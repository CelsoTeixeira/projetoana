﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//We use this to load all the system and not stay with a black image in the beginning.

public class LoadSystem : MonoBehaviour
{
    public Slider LoadingBar;
    public Image LoadingImage;

    public GameObject LoadGameObject;

    [Tooltip("What level we want to load.")]
    public int LevelToLoad = 1;
    
    [Tooltip("Mark this if the LoadAsync should be called automatic on Awake.")]
    public bool AutoCall = true;

    public float MinimunTimeToWait = 4f;   //This is just a value before we start loading stuff.

    //This is our operation.
    private AsyncOperation async;

    void Awake()
    {
        //If this is true, we just start our load operation.
        if (AutoCall)
        {
            LoadGameObject.SetActive(true);
        
            StartLoadAsync(LevelToLoad);    
        }

        //We could do some GC and UnloadAssets here if it's necessary.
    }

    public void StartLoadAsync(int levelToLoad)
    {
        //We could do some animation stuff here.

        StartCoroutine(Wait());
    }

    private IEnumerator Load(int levelToLoad)
    {
        async = Application.LoadLevelAsync(levelToLoad);

        //While the level is not load we refresh the progress.
        while (!async.isDone)
        {
            LoadingBar.value = async.progress;
            yield return null;
        }


    }

    private IEnumerator Wait()
    {
        LoadingBar.value = 0;

        yield return new WaitForSeconds(MinimunTimeToWait);

        StartCoroutine(Load(1));
    }
}