﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour
{
#region Singleton

    public static SoundController Instance { get; private set; }

    private void SingletonSetup()
    {
        if (Instance != this && Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

#endregion

    public List<AudioClip> AmbienceSounds = new List<AudioClip>();
    public List<AudioClip> FeedbackSounds = new List<AudioClip>();

    private AudioSource _ambienceAudioSource;
    private AudioSource _feedbackAuidoSource;
    

    void Awake()
    {
        SingletonSetup();

        _ambienceAudioSource = GameObject.FindGameObjectWithTag(GeneralInformationHolder.AmbienceSound).GetComponent<AudioSource>();
        _feedbackAuidoSource = GameObject.FindGameObjectWithTag(GeneralInformationHolder.FeedbackSound).GetComponent<AudioSource>();


    }

    void Start()
    {
        _ambienceAudioSource.clip = AmbienceSounds[0];

        _ambienceAudioSource.loop = true;
        _ambienceAudioSource.volume = 0.05f;
        _ambienceAudioSource.Play();
    }

    public void PlayFeedbackSelectionSound()
    {
        _feedbackAuidoSource.clip = FeedbackSounds[1];
        _feedbackAuidoSource.Play();
    }

    public void PlayFeedbackFowardSound()
    {
        _feedbackAuidoSource.clip = FeedbackSounds[0];
        _feedbackAuidoSource.Play();
    }
}
