﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class DeletePlayerPrefs : MonoBehaviour
{
    [MenuItem("Edit/Reset Playerprefs")]
    public static void DeletePlayerPref()
    {
        PlayerPrefs.DeleteAll();
    }
}