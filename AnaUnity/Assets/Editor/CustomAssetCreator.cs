﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class CustomAssetCreator : MonoBehaviour 
{
    [MenuItem("Assets/Create/Panel Information")]
    public static void CreateAsset()
    {
        CustomAssetUtility.CreateAsset<CustomPanelInformation>();
    }
}
